{-|
  number_guessing - guess a number in a given range

  Usage:    number_guessing [MIN-VAL] [MAX-VAL]

  Examples: number_guessing         # -> range:  1 .. 100 (default)
            number_guessing 50      # -> range: 50 .. 100 (MAX-VAL is default)
            number_guessing d  200  # -> range:  1 .. 200 (MIN-VAL is default)
            number_guessing 10 150  # -> range: 10 .. 150

  Compile:  ghc --make -Wall number_guessing.hs
  Lint:     hlint -s number_guessing.hs

  Author:   haskell@wu6ch.de

  Inspired by:

  "Know what your functions are doing? - Side effects in 12+ languages"
  (https://youtu.be/olISecOUX1g) -> 15:05 Haskell

  "contextfreecode / procfun / guess.hs"
  (https://github.com/contextfreecode/procfun/blob/main/guess.hs)
-}


import Control.Applicative ((<|>))

import Control.Monad.State
  ( StateT
  , get
  , gets
  , liftIO
  , modify
  , runStateT
  , zipWithM
  )

import Data.Bifunctor (bimap)
import Data.Bool (bool)
import Data.Maybe (fromMaybe)

import System.Environment (getArgs)
import System.IO (hFlush, hPutStr, stderr, stdout)
import System.Random (randomRIO)

import Text.Read (readMaybe)


type GuessRange = (Int, Int)
type Guesses    = Int
type GameValue  = Guesses

data GameState =
  GameState
    { gsGuessRange :: GuessRange
    , gsGuessVal   :: Int
    , gsInvalidCnt :: Guesses
    , gsLowCnt     :: Guesses
    , gsHighCnt    :: Guesses
    }


defaultGuessRange :: GuessRange
defaultGuessRange = (1, 100)


handleInvalid :: String -> StateT GameState IO GameValue
handleInvalid s = do
  modify (\state -> state {gsInvalidCnt = gsInvalidCnt state + 1})
  liftIO $ putStrLn $ "I didn't understand '" <> s <> "'"
  playGame


handleTooLow :: Int -> StateT GameState IO GameValue
handleTooLow n = do
  modify (\state -> state {gsLowCnt = gsLowCnt state + 1})
  liftIO $ putStrLn $ show n <> " is too low"
  playGame


handleGuessed :: StateT GameState IO GameValue
handleGuessed = do
  gameState <- get
  liftIO $ putStrLn $ show (gsGuessVal gameState) <> " is the answer!"
  return $ 1 + sum ([gsInvalidCnt, gsLowCnt, gsHighCnt] <*> pure gameState)


handleTooHigh :: Int -> StateT GameState IO GameValue
handleTooHigh n = do
  modify (\state -> state {gsHighCnt = gsHighCnt state + 1})
  liftIO $ putStrLn $ show n <> " is too high"
  playGame


handleValid :: Int -> StateT GameState IO GameValue
handleValid n = do
  guessVal <- gets gsGuessVal
  case compare n guessVal of
    LT -> handleTooLow  n
    EQ -> handleGuessed
    GT -> handleTooHigh n


playGame :: StateT GameState IO GameValue
playGame = do
  guessRange <- gets gsGuessRange
  let (l, h) = bimap show show guessRange
  liftIO $ putStr $ mconcat ["Guess a number between ", l, " and ", h, ": "]
  liftIO $ hFlush stdout
  s <- liftIO getLine
  case readMaybe s of
    Nothing -> handleInvalid s
    Just n  -> handleValid   n


readGuessRange :: [String] -> GuessRange
readGuessRange = listToPair . fromMaybe [] . convert
  where
    listToPair :: [Int] -> GuessRange
    listToPair (x:y:_) = (x, y)
    listToPair (x:_)   = (x, snd defaultGuessRange)
    listToPair _       = defaultGuessRange
    convert :: [String] -> Maybe [Int]
    convert xs = zipWithM (<|>) (readMaybeInts xs) (Just <$> defRange)
    readMaybeInts :: [String] -> [Maybe Int]
    readMaybeInts = map readMaybe . take 2
    defRange = [fst defaultGuessRange, snd defaultGuessRange]


printResult :: (GameValue, GameState) -> IO ()
printResult (guesses, endState) = do
  putStrLn $ "Finished in " <> showGuesses guesses
  hPutStr stderr $
    unlines
      [ "Total input errors: "           <> show (gsInvalidCnt endState)
      , showGuesses (gsLowCnt  endState) <> " \"too low\""
      , showGuesses (gsHighCnt endState) <> " \"too high\""
      ]
  where
    showGuesses :: Guesses -> String
    showGuesses g = mconcat [show g, " guess", bool "es" "" (g == 1)]


main :: IO ()
main = do
  args <- getArgs
  let guessRange = readGuessRange args
  guessVal <- randomRIO guessRange
  let initialGameState = GameState guessRange guessVal 0 0 0
  runStateT playGame initialGameState >>= printResult


-- EOF
